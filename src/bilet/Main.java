package bilet;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Ticket> ticketList = null;
        String komenda;
        do {
            System.out.println("Podaj komende:");
            komenda = scanner.next();       // jedno słowo
            if (komenda.equalsIgnoreCase("dodaja")) {   // array list
                int ilosc = scanner.nextInt();
                ticketList = dodajDoArrayList(ilosc);
            } else if (komenda.equalsIgnoreCase("dodajl")) { // linked list
                int ilosc = scanner.nextInt();
                ticketList = dodajDoLinkedList(ilosc);
            } else if (komenda.equalsIgnoreCase("czysc")) { // usun wszystko z listy
                ticketList.clear();
            } else if (komenda.equalsIgnoreCase("iteruj")) { // iteruj
                iteruj(ticketList);
            } else if (komenda.equalsIgnoreCase("gc")) { // uzyj garbedż kolektora
                Runtime.getRuntime().gc();
            }
        } while (!komenda.equalsIgnoreCase("quit"));
    }
    public static void iteruj(List<Ticket> list) {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).getId();
        }
    }
    public static List<Ticket> dodajDoArrayList(int liczba) {
        List<Ticket> ticketList = new ArrayList<>();
        for (int i = 0; i < liczba; i++) {
            ticketList.add(new Ticket());
        }
        return ticketList;
    }
    public static List<Ticket> dodajDoLinkedList(int liczba) {
        List<Ticket> ticketList = new LinkedList<>();
        for (int i = 0; i < liczba; i++) {
            ticketList.add(new Ticket());
        }
        return ticketList;
    }
}
