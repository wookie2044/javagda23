package com.sda.javac;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ilość powtórzeń ");
        int wartosc = scanner.nextInt();

        for (int i = 0; i< wartosc; i++){
            System.out.println("Hello World!!");
        }
    }
}
// javac src/com/sda/javac/Main.java
// java -cp src com.sda.javac.Main
// javap -verbose -private src/com/sda/javac/Main.class

// Kompilacja i otwieranie pliku w Bashu