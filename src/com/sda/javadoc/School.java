package com.sda.javadoc;

import java.util.List;
import java.util.Set;

/**
 * create a new interface.
 */
public interface School
/**
 * Adds student given in a parameter to the "School"
 * @param student - student added to "School"
 */
{
    void addStudent(Student student);

    /**
     * Creates and adds "Student" object to "School" interface. Method requeires all parameters to creata a single "Student"
     * than adds newly created student to "School".
     * @param firstName - first name of "Student" that is going to be created.
     * @param lastName - last name of "Student" that is going to be created.
     * @param age - age of "Student" that is going to be created.
     * @param courses - courses attended by "Student" that is going to be created.
     */
    void addStudent(String firstName, String lastName, int age, Set<Course> courses);
    Student getStudentByFirstNameAndLastName(String firstName, String lastName);
    List<Student> getStudentsByCourse(Course course);
}