package com.sda.javadoc;

/**
 * This Enum represetns every type of Course
 *
 * */

public enum Course {
    /**
     * this value represents backend developer course.
     */
    BACKEND_DEVELOPER("Backend Developer"),
    /**
     * this value frontend development course with technologies such as Angular,React.
     */
    FRONTEND_DEVELOPER("Frontend Developer"),
    /**
     * this value is mockup.
     */
    AI_ENGINEER("AI Engineer"),
    /**
     * This value looks beautiful and means nothing.
     */

    DEV_OPS("Dev ops");

    private String courseName;

    /**
     * Course constructor, creats a course with description given a parameter.
     * @param courseName - course description/name.
     */
    Course(String courseName) {
        this.courseName = courseName;
    }

    /**
     * Returns course description name.
     * @return course name.
     */


    public String getCourseName() {
        return courseName;
    }
}