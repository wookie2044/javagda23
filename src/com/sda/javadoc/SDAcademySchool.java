package com.sda.javadoc;

import java.util.List;
import java.util.Set;

/**
 * this class implements a "School" interface.
 */
public class SDAcademySchool implements School
/**
 * Overriding method.
 */
{
    @Override
    public void addStudent(Student student) {}
    @Override
    public void addStudent(String firstName, String lastName, int age, Set<Course> courses) {}
    @Override
    public Student getStudentByFirstNameAndLastName(String firstName, String lastName) {
        return null;
    }
    @Override
    public List<Student> getStudentsByCourse(Course course) {
        return null;
    }
}