package com.sda.javadoc;

import java.util.Set;

/**
 * this is a public class.
 */
public class Student {
    /**
     * this is a private and final (not changable) String.
     */
    private final String firstName;
    private final String lastName;
    private final int age;
    private final Set<Course> courses;
    public Student(String firstName, String lastName, int age, Set<Course> courses)
    /**
     * those are a constructors.
     */
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.courses = courses;
    }

    /**
     * Those are a Getters
     * @return value int, String
     */
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public int getAge() {
        return age;
    }
    public Set<Course> getCourses() {
        return courses;
    }
}